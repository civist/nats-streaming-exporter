# NATS streaming server Prometheus exporter

This is a simple [Prometheus](https://prometheus.io/) exporter for [NATS streaming server](https://nats.io/documentation/streaming/nats-streaming-intro/) metrics.

## Installation

### Using `go get`

```
go get gitlab.com/civist/nats-streaming-exporter
```

### Using Docker

```
docker pull registry.gitlab.com/civist/nats-streaming-exporter
docker run --rm -p 9275:9275 registry.gitlab.com/civist/nats-streaming-exporter -nats-uri http://nats-streaming:8222
```

## Running

```
nats-streaming-exporter --help

Usage of nats-streaming-exporter:
  -listen string
        Address to listen on. (default ":9275")
  -log.format value
        Set the log target and format. Example: "logger:syslog?appname=bob&local=7" or "logger:stdout?json=true" (default "logger:stderr")
  -log.level value
        Only log messages with the given severity or above. Valid levels: [debug, info, warn, error, fatal] (default "info")
  -metrics-path string
        Path under which to expose metrics. (default "/metrics")
  -nats-uri string
        Base URI on which to scrape NATS server. (default "http://localhost:8222/")
  -timeout duration
        Timeout for collecting NATS metrics. (default 5s)
```
