// Copyright (c) 2017 Civist UG (haftungsbeschränkt)
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"gitlab.com/civist/nats-streaming-exporter/exporter"
)

func main() {
	var (
		listen      = flag.String("listen", ":9275", "Address to listen on.")
		metricsPath = flag.String("metrics-path", "/metrics", "Path under which to expose metrics.")
		natsURI     = flag.String("nats-uri", "http://localhost:8222/", "Base URI on which to scrape NATS server.")
		timeout     = flag.Duration("timeout", 5*time.Second, "Timeout for collecting NATS metrics.")
	)
	flag.Parse()

	exp, err := exporter.New(*natsURI, *timeout)
	if err != nil {
		log.Fatal(err)
	}
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)

	prometheus.MustRegister(exp)

	// Setup HTTP server
	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>NATS Exporter</title></head>
             <body>
             <h1>NATS Streaming Server Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})

	go func() {
		log.Infof("Starting Server: %s", *listen)
		log.Fatal(http.ListenAndServe(*listen, nil))
	}()

	s := <-sigs
	log.Infof("Received %v, terminating", s)
	os.Exit(0)
}
