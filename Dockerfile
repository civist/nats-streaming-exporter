FROM golang:latest AS builder
WORKDIR /go/src/gitlab.com/civist/nats-streaming-exporter
RUN go get -d -v github.com/prometheus/client_golang/prometheus
RUN go get -d -v github.com/nats-io/nats-streaming-server/server
COPY . .
RUN go get -d -v
RUN [ -d build ] || mkdir build
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -tags netgo -o build/nats-streaming-exporter .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/gitlab.com/civist/nats-streaming-exporter/build/nats-streaming-exporter .
CMD ["/nats-streaming-exporter"]