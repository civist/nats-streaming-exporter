// Copyright (c) 2017 Civist UG (haftungsbeschränkt)
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

package exporter

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/nats-io/nats-streaming-server/server"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/log"
)

var _ prometheus.Collector = &Exporter{}

const namespace = "natsstreaming"

// Exporter collects NATS streaming server metrics and exposes them using prometheus
type Exporter struct {
	URI     *url.URL
	Timeout time.Duration

	up                              prometheus.Gauge
	totalScrapes, jsonParseFailures prometheus.Counter

	clientsTotal, channelsTotal, storeMessagesTotal, storeMessagesBytes      *prometheus.Desc
	subscriptionsTotal, subscriptionsPendingTotal, subscriptionsStalledTotal *prometheus.Desc
	messagesTotal, messagesBytes                                             *prometheus.Desc
}

// New returns an initialized Exporter
func New(uri string, timeout time.Duration) (*Exporter, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}
	return &Exporter{
		URI:     u,
		Timeout: timeout,
		up: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "up",
			Help:      "Was the last scrape of nats-streaming successful.",
		}),
		totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "exporter_total_scrapes",
			Help:      "Current total nats-streaming scrapes.",
		}),
		jsonParseFailures: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "exporter_json_parse_failures",
			Help:      "Number of errors while parsing JSON.",
		}),
		clientsTotal:              newDesc("clients", "Number of currently connected clients.", nil),
		channelsTotal:             newDesc("channels", "Current number of channels.", nil),
		storeMessagesTotal:        newDesc("store_messages", "Current number of messages in the store.", nil),
		storeMessagesBytes:        newDesc("store_messages_bytes", "Total size of the messages in the store.", nil),
		subscriptionsTotal:        newDesc("subscriptions", "Number of subscriptions.", []string{"channel", "client"}),
		subscriptionsPendingTotal: newDesc("subscriptions_pending", "Number of pending messages.", []string{"channel", "client"}),
		subscriptionsStalledTotal: newDesc("subscriptions_stalled", "Number of stalled subscriptions.", []string{"channel", "client"}),
		messagesTotal:             newDesc("messages", "Number of messages.", []string{"channel"}),
		messagesBytes:             newDesc("messages_bytes", "Size of the messages.", []string{"channel"}),
	}, nil
}

func newDesc(name, help string, variableLabels []string) *prometheus.Desc {
	return prometheus.NewDesc(prometheus.BuildFQName(namespace, "", name), help, variableLabels, nil)
}

func (e *Exporter) fetch(ctx context.Context, path string, values url.Values, data interface{}) error {
	uri := e.URI.ResolveReference(&url.URL{Path: path, RawQuery: values.Encode()})
	req, err := http.NewRequest(http.MethodGet, uri.String(), nil)
	if err != nil {
		return err
	}
	rsp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		e.up.Set(0)
		return err
	}
	defer rsp.Body.Close()
	err = json.NewDecoder(rsp.Body).Decode(data)
	if err != nil {
		e.jsonParseFailures.Inc()
	}
	return err
}

func (e *Exporter) collectServerStats(ctx context.Context, ch chan<- prometheus.Metric) {
	var stats server.Serverz
	if err := e.fetch(ctx, server.ServerPath, nil, &stats); err != nil {
		log.Error(err)
		return
	}
	ch <- prometheus.MustNewConstMetric(e.clientsTotal, prometheus.GaugeValue, float64(stats.Clients))
	ch <- prometheus.MustNewConstMetric(e.channelsTotal, prometheus.GaugeValue, float64(stats.Channels))
	ch <- prometheus.MustNewConstMetric(e.storeMessagesTotal, prometheus.GaugeValue, float64(stats.TotalMsgs))
	ch <- prometheus.MustNewConstMetric(e.storeMessagesBytes, prometheus.GaugeValue, float64(stats.TotalBytes))
}

func (e *Exporter) collectClientStats(ctx context.Context, ch chan<- prometheus.Metric) {
	var stats server.Clientsz
	if err := e.fetch(ctx, server.ClientsPath, url.Values{"subs": {"1"}}, &stats); err != nil {
		log.Error(err)
		return
	}
	for _, client := range stats.Clients {
		for channelName, subs := range client.Subscriptions {
			pending := 0
			stalled := 0
			for _, sub := range subs {
				pending += sub.PendingCount
				if sub.IsStalled {
					stalled++
				}
			}
			ch <- prometheus.MustNewConstMetric(e.subscriptionsTotal, prometheus.GaugeValue, float64(len(subs)), channelName, client.ID)
			ch <- prometheus.MustNewConstMetric(e.subscriptionsPendingTotal, prometheus.GaugeValue, float64(pending), channelName, client.ID)
			ch <- prometheus.MustNewConstMetric(e.subscriptionsStalledTotal, prometheus.GaugeValue, float64(stalled), channelName, client.ID)
		}
	}
}

func (e *Exporter) collectChannelStats(ctx context.Context, ch chan<- prometheus.Metric) {
	var stats server.Channelsz
	if err := e.fetch(ctx, server.ChannelsPath, url.Values{"subs": {"1"}}, &stats); err != nil {
		log.Error(err)
		return
	}
	for _, c := range stats.Channels {
		ch <- prometheus.MustNewConstMetric(e.messagesTotal, prometheus.GaugeValue, float64(c.Msgs), c.Name)
		ch <- prometheus.MustNewConstMetric(e.messagesBytes, prometheus.GaugeValue, float64(c.Bytes), c.Name)
	}
}

// Describe describes all the metrics exported by the NATS streaming server exporter. It
// implements prometheus.Collector.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.clientsTotal
	ch <- e.channelsTotal
	ch <- e.storeMessagesTotal
	ch <- e.storeMessagesBytes
	ch <- e.subscriptionsTotal
	ch <- e.subscriptionsPendingTotal
	ch <- e.subscriptionsStalledTotal
	ch <- e.messagesTotal
	ch <- e.messagesBytes
}

// Collect fetches the stats from configured NATS streaming server location and delivers them
// as Prometheus metrics. It implements prometheus.Collector.
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.up.Set(1)
	ctx, cancel := context.WithTimeout(context.Background(), e.Timeout)
	defer cancel()
	e.totalScrapes.Inc()

	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		defer wg.Done()
		e.collectServerStats(ctx, ch)
	}()
	go func() {
		defer wg.Done()
		e.collectClientStats(ctx, ch)
	}()
	go func() {
		defer wg.Done()
		e.collectChannelStats(ctx, ch)
	}()
	wg.Wait()

	e.up.Collect(ch)
	e.totalScrapes.Collect(ch)
	e.jsonParseFailures.Collect(ch)
}
